var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Создаем массивы students и points

var students = studentsAndPoints.filter(function (student){
	return (typeof student === 'string');
});

var points = studentsAndPoints.filter(function (point){
	return (typeof point === 'number');
});

console.log('');

//Выводим список студентов

console.log("Список студентов:");
students.forEach(function(student, i){
	console.log("Студент %s набрал %d баллов", student, points[i]);
});

console.log('');

//Ищем информацию о студенте, набравшем наибольшее количество баллов

var maxIndex;
var max = points.reduce(function (max, current, i){
	if ( current > max ) {
    maxIndex = i;
    return current;
  }
  return max;
})

console.log("Студент, набравший маскимальный балл: %s (%d баллов)", students[maxIndex], max);

console.log('');

/*Увеличиваем баллы
  Исправил в этом месте*/

points = students.map(function (student, i){
  if ( student == 'Ирина Овчинникова'){
  	console.log("У %s было %d баллов", student, points[i]);
  	points[i] +=30;
    console.log("У %s стало %d баллов", student, points[i]);
  }
  if ( student === 'Александр Малов'){
  	console.log("У %s было %d баллов", student, points[i]);
  	points[i] +=30;
    console.log("У %s стало %d баллов", student, points[i]);
  }
  return points[i];
});